from django import forms
from . import models

class SearchBar(forms.Form):
	query = forms.CharField(label='Nama Buku:',
							 max_length=200,
							 widget=forms.TextInput(attrs={'id':'query-search',
							 							   'placeholder':'Ex : Python',
							 							   'type':'query'}))