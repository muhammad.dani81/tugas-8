from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse

import requests

from .forms import SearchBar

def home(request):
	response = {'searchbar':SearchBar}
	return render(request, 'tugas8.html', response)

def get_json(request):
    query = request.GET.get('query')
    url = "https://www.googleapis.com/books/v1/volumes?q=" + query

    return JsonResponse(requests.get(url).json())
