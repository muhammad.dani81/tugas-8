import unittest
import time

from django.test import TestCase, Client
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from .forms import SearchBar
from .views import home, get_json

class UnitTest(TestCase):

	def test_url_exists(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	def test_url_that_does_not_exist(self):
		response = Client().get('/nothing/')
		self.assertEqual(response.status_code, 404)

	def test_use_form_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'tugas8.html')

	def test_nama_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('Nama Buku:', content)
	def test_form_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<form', content)
	def test_button_is_inside_template(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<button', content)
		self.assertIn('Cari', content)

	def test_page_uses_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, home) #Views's function

class SeleniumFunctionalTest(TestCase):

	def setUp(self):
		options = Options()
		options.add_argument('--headless')
		self.selenium = webdriver.Firefox(firefox_options=options)
		super(SeleniumFunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(SeleniumFunctionalTest,self).tearDown()

	def test_inserting_data_to_form(self):
		#Inserting one data
		self.selenium.get('http://127.0.0.1:8000/')
		time.sleep(10)

		form_search = self.selenium.find_element_by_id("query-search")
		form_search.send_keys("Troops")
		button_search = self.selenium.find_element_by_id("search")
		button_search.click()
		time.sleep(10)

		self.assertIn("Troops", self.selenium.page_source)