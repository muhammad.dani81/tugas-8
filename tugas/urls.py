from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from .views import home, get_json

urlpatterns = [
    path('', home, name='home'),
    path('get_json/', get_json, name='get_json'),
]